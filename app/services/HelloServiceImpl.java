package services;

import doma.AppConfig;
import doma.user.User;
import doma.user.UserDao;
import doma.user.UserDaoImpl;
import doma.user.domain.Email;
import play.Logger;

import java.sql.SQLException;
import java.util.List;


public class HelloServiceImpl implements HelloService {

    private UserDao userDao = new UserDaoImpl();

    @Override
    public String sayHello() {

        List<User> list = userDao.select();

        String s = "";

        for(User user : list){
            s += String.format("Hello! %s", user.getFullname());
        }

        return s;
    }

    @Override
    public String registDataWithTran() throws SQLException {

        String m = "----------------------------------------"   + System.getProperty("line.separator");
        Logger.debug("----------------------------------------");

        m += "10件のユーザーをトランザクションで登録します。"   + System.getProperty("line.separator");
        Logger.debug("10件のユーザーをトランザクションで登録します。");

        // トランザクションの開始
        AppConfig appConfig = new AppConfig();
        javax.sql.DataSource dataSource = appConfig.getDataSource();

        //Logger.debug(dataSource.getConnection().getClientInfo().toString());


        try{

            Regist10User();

            dataSource.getConnection().commit();
        }catch(Exception ex){
            Logger.debug(ex.getMessage());

            m += "例外をキャッチしました。明示的にロールバックを行います。"+ System.getProperty("line.separator");

            dataSource.getConnection().rollback();
        }


        m += "----------------------------------------" + System.getProperty("line.separator");
        Logger.debug("----------------------------------------");

        return m;
    }

    @Override
    public String registDataNotTran() throws SQLException {

        String m = "----------------------------------------"   + System.getProperty("line.separator");
        Logger.debug("----------------------------------------");

        m += "10件のユーザーをトランザクションで登録します。"   + System.getProperty("line.separator");
        Logger.debug("10件のユーザーをトランザクションで登録します。");

        Regist10User();

        m += "----------------------------------------"   + System.getProperty("line.separator");
        Logger.debug("----------------------------------------");

        return m;
    }

    private void Regist10User() {
        for(int i = 0; i < 10; i++){
            User user = new User();

            Email email = Email.of("user" + i + "@hoge.local");
            user.setEmail(email);
            user.setFullname("ななし　ごんべ" + i);
            user.setIsAdmin(i % 2==0);

            if(i == 2){

                String s = "";

                for(int j = 0; j < 300; j++){
                    s += j;
                }

                user.setFullname(s);
            }

            userDao.insert(user);
        }
    }


}
