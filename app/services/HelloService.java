package services;

import java.sql.SQLException;

public interface HelloService {

    String sayHello();

    String registDataWithTran() throws SQLException;
    String registDataNotTran() throws SQLException;

}
