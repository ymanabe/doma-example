package initialize;


import com.google.inject.AbstractModule;
import doma.user.UserDao;
import doma.user.UserDaoImpl;

public class GuiceModule extends AbstractModule {

    @Override
    protected void configure() {
//        bind(HelloService.class).to(HelloServiceImpl.class);
        bind(UserDao.class).to(UserDaoImpl.class);
    }

}