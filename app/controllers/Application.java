package controllers;

import doma.AppConfig;
import doma.user.User;
import doma.user.UserDao;
import doma.user.UserDaoImpl;
import doma.user.domain.Email;
import play.Logger;
import play.mvc.Controller;
import services.HelloService;
import services.HelloServiceImpl;

import java.sql.SQLException;

/**
 * サンプルアプリケーションのコントローラ
 */
public class Application extends Controller {


    public static void index() {

        HelloService helloService = new HelloServiceImpl();

        render();
    }

    public static void execTranAtServiceThrowExceptionData3() throws SQLException {
        HelloService helloService = new HelloServiceImpl();

        String m = "サービス内でトランザクション制御を行います。登録処理は3件目で例外がスローされます。"+ System.getProperty("line.separator");;

        UserDao dao = new UserDaoImpl();
        Logger.debug("処理件数:" + dao.select().size());
        m += "処理前件数:" + dao.select().size() + System.getProperty("line.separator");

        m += helloService.registDataWithTran();

        Logger.debug("処理件数:" + dao.select().size());
        m += "処理後件数:" + dao.select().size() + System.getProperty("line.separator");

        renderText(m);
    }

    public static void execWithoutTranAtServiceThrowExceptionData3() throws SQLException {
        HelloService helloService = new HelloServiceImpl();

        String m = "サービス内でデータの登録処理を行います。トランザクションは使用しません。登録処理は3件目で例外がスローされます。"+ System.getProperty("line.separator");;

        UserDao dao = new UserDaoImpl();
        Logger.debug("処理件数:" + dao.select().size());
        m += "処理前件数:" + dao.select().size() + System.getProperty("line.separator");


        try{
            m+=helloService.registDataNotTran();
            throw new SQLException();
        }catch (Exception ex){
            m+= "例外をキャッチしました。" + System.getProperty("line.separator");
            Logger.debug("例外をキャッチしました.");
        }

        Logger.debug("処理件数:" + dao.select().size());
        m += "処理後件数:" + dao.select().size() + System.getProperty("line.separator");

        renderText(m);
    }


    public static void execWithoutTranAtControllerThrowExceptionData3() throws SQLException {
        HelloService helloService = new HelloServiceImpl();

        String m = "コントローラー内でデータの登録処理を行います。トランザクションは使用しません。登録処理は3件目で例外がスローされます。"+ System.getProperty("line.separator");;
        try{

            boolean b = AppConfig.getLocalTransaction().isActive();
            m += "localTransaction.isActive:" + b + System.getProperty("line.separator");

            UserDao dao = new UserDaoImpl();
            Logger.debug("処理前件数:" + dao.select().size());
            m += "処理前件数:" + dao.select().size() + System.getProperty("line.separator");

            Regist10User();
            throw new SQLException();
        }catch (Exception ex){
            //AppConfig.getLocalTransaction().rollback();
            m+= "例外をキャッチしました。" + System.getProperty("line.separator");
            Logger.debug("例外をキャッチしました.");
        }

        UserDao dao = new UserDaoImpl();
        Logger.debug("処理件数:" + dao.select().size());
        m += "処理後件数:" + dao.select().size() + System.getProperty("line.separator");

        renderText(m);
    }

    private static void Regist10User() {

        for(int i = 0; i < 10; i++){
            User user = new User();

            Email email = Email.of("user" + i + "@hoge.local");
            user.setEmail(email);
            user.setFullname("ななし　ごんべ" + i);
            user.setIsAdmin(i % 2==0);

            if(i == 2){

                String s = "";

                for(int j = 0; j < 300; j++){
                    s += j;
                }

                user.setFullname(s);
            }


            new UserDaoImpl().insert(user);
        }
    }
}