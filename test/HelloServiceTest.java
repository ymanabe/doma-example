import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import doma.user.User;
import doma.user.UserDao;
import mockit.Expectations;
import mockit.Mocked;
import org.junit.Before;
import org.junit.Test;
import services.HelloService;
import services.HelloServiceImpl;

import java.util.Arrays;

import static org.seasar.doma.internal.util.AssertionUtil.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: ymanabe
 * Date: 2013/07/14
 * Time: 8:42
 */
public class HelloServiceTest {

    private Injector injector;

    @Mocked
    private UserDao userDao;

    @Before
    public void setUp() {

    }

    @Test
    public void testSample(){

        final User user = new User();
        user.setFullname("山田 太郎");

        new Expectations(){{
           userDao.select();
            result = Arrays.asList(user);
        }};

        injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(UserDao.class).toInstance(userDao);
            }
        });

        HelloService service = injector.getInstance(HelloServiceImpl.class);

        String s = service.sayHello();
        assertEquals("Hello! 山田 太郎", s);
    }
}
